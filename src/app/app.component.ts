import { Component } from '@angular/core';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Http } from '@angular/http';
import { CookieOptionsArgs } from 'angular2-cookie/services/cookie-options-args.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements CookieOptionsArgs {


  title = 'app works!';
  constructor(private cookie: CookieService, private http: Http) { }
  ngOnInit() {
    const args: CookieOptionsArgs = {
      path: '/',
      domain: "localhost",
      expires: "8 5 2017 01:40:12",
      secure: false
    };

    this.cookie.put("testing", "123456789", args);
    if (this.cookie.get("testing") !== undefined) {
      console.log("cookie found: " + " " + this.cookie.get("test"));
    }
    else {
      console.log("cookie not found");
      window.location.href = "http://login.rewardle.com/";
    }
  }
}
