#!/bin/bash

source ~/.bashrc
docker pull rewardle/deployer
echo $STACKNAME

echo ~
npm install
ng build
ls -lart
zip -r dist.zip dist

ls -lart

pwd
docker run -it \
  -v `pwd`:/app \
  --entrypoint=aws rewardle/deployer \
  s3 cp --region=ap-southeast-2 --acl public-read \
  /app/dist.zip s3://rewardleplaybuilds/builds/rewardle-test-cookie/$BUILDKITE_BUILD_NUMBER/



