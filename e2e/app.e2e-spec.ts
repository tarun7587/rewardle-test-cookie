import { TestCookiePage } from './app.po';

describe('test-cookie App', () => {
  let page: TestCookiePage;

  beforeEach(() => {
    page = new TestCookiePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
